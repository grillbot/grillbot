﻿namespace GrillBot.Database.Enums;

public enum ExplicitPermissionState
{
    /// <summary>
    /// Access to command is allowed.
    /// </summary>
    Allowed,

    /// <summary>
    /// Access to command is banned.
    /// </summary>
    Banned
}
