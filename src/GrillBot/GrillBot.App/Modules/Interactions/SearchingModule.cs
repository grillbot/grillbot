﻿using Discord.Interactions;
using GrillBot.App.Infrastructure.Commands;
using GrillBot.App.Infrastructure.Preconditions.Interactions;
using GrillBot.App.Modules.Implementations.Searching;
using GrillBot.App.Services;
using GrillBot.Common.Helpers;
using GrillBot.Data.Models.API.Searching;
using Microsoft.Extensions.DependencyInjection;

namespace GrillBot.App.Modules.Interactions;

[RequireUserPerms]
[Group("search", "Searching")]
public class SearchingModule : InteractionsModuleBase
{
    private SearchingService SearchingService { get; }

    public SearchingModule(SearchingService searchingService, IServiceProvider serviceProvider) : base(serviceProvider)
    {
        SearchingService = searchingService;
    }

    [SlashCommand("list", "Current search.")]
    public async Task SearchingListAsync(
        [Summary("channel", "The channel you want to find something in.")]
        ITextChannel channel = null,
        [Summary("substring", "Search substring")] [Discord.Interactions.MaxLength(50)]
        string query = null
    )
    {
        channel ??= (ITextChannel)Context.Channel;

        var parameters = new GetSearchingListParams
        {
            Pagination = { Page = 0, PageSize = EmbedBuilder.MaxFieldCount },
            Sort = { Descending = false, OrderBy = "Id" },
            ChannelId = channel.Id.ToString(),
            GuildId = Context.Guild.Id.ToString(),
            MessageQuery = query
        };

        using var scope = ServiceProvider.CreateScope();

        var action = ServiceProvider.GetRequiredService<Actions.Api.V1.Searching.GetSearchingList>();
        action.UpdateContext(Locale, Context.User);
        
        var list = await action.ProcessAsync(parameters);
        var pagesCount = (int)Math.Ceiling(list.TotalItemsCount / (double)EmbedBuilder.MaxFieldCount);

        var embed = new EmbedBuilder()
            .WithSearching(list, channel, Context.Guild, 0, Context.User, query);

        var components = ComponentsHelper.CreatePaginationComponents(0, pagesCount, "search");
        await SetResponseAsync(embed: embed.Build(), components: components);
    }

    [RequireSameUserAsAuthor]
    [ComponentInteraction("search:*", ignoreGroupNames: true)]
    public async Task HandleSearchingListPaginationAsync(int page)
    {
        var handler = new SearchingPaginationHandler(Context.Client, ServiceProvider, page);
        await handler.ProcessAsync(Context);
    }

    [SlashCommand("create", "Create a new search.")]
    public async Task CreateSearchAsync(
        [Summary("message", "Message")] string message
    )
    {
        try
        {
            await SearchingService.CreateAsync(Context.Guild, Context.User as IGuildUser, Context.Channel as IGuildChannel, message);
            await SetResponseAsync(GetText(nameof(CreateSearchAsync), "Success"));
        }
        catch (ValidationException ex)
        {
            await SetResponseAsync(ex.Message);
        }
    }

    [SlashCommand("remove", "Deletes the search")]
    public async Task RemoveSearchAsync(
        [Autocomplete(typeof(SearchingAutoCompleteHandler))] [Summary("ident", "Search identification")]
        long ident
    )
    {
        try
        {
            await SearchingService.RemoveSearchAsync(ident, Context.User as IGuildUser);
            await SetResponseAsync(GetText(nameof(RemoveSearchAsync), "Success"));
        }
        catch (UnauthorizedAccessException ex)
        {
            await SetResponseAsync(ex.Message);
        }
    }
}
